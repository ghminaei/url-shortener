from locust import HttpUser, TaskSet, task
import string, random

USER_CREDENTIALS = [
    ("user1", "hardPassword12"),
    ("user2", "hardPassword12"),
    ("user3", "hardPassword12"),
    ("user4", "hardPassword12"),
    ("user5", "hardPassword12"),
    ("user6", "hardPassword12"),
    ("user7", "hardPassword12"),
    ("user8", "hardPassword12"),
    ("user9", "hardPassword12"),
    ("user10", "hardPassword12"),
]

SHORT_URLS = []

CSRF_TOKENS = []

class UserLogedin(TaskSet):
    def on_start(self):
        user, password = random.choice(USER_CREDENTIALS)
        response = self.client.post('/uApi/rest-auth/login/',
                            {'username': user, 'password': password})
        csrftoken = response.cookies['csrftoken']
        self.csrftoken = csrftoken


    @task(5)
    def makeNewUrl(self):
        N = 100
        M = 5
        s = string.ascii_uppercase + string.ascii_lowercase + string.digits
        newUrl = ''.join(random.choices(s, k=N))
        longUrl = newUrl + ".com"

        sugUrl = newUrl = ''.join(random.choices(s, k=M))
        response = self.client.post(
            "/uApi/", {
                "longUrl": longUrl, 
                "suggestedUrl": sugUrl
                }, 
            headers={'X-CSRFToken': self.csrftoken}
        )
        jsonVar = response.json()
        SHORT_URLS.append(jsonVar['shortUrl'])
    
    @task(10)
    def getUserReport(self):
        self.client.get("/report/per-user/")
    
    @task(10)
    def getUrlReport(self):
        self.client.get("/report/per-url/")


class userRedirect(TaskSet):
    @task(85)
    def redirect(self):
        newUrl = ''.join(random.choices(SHORT_URLS))
        req = "/uApi/r/" + newUrl
        self.client.get(req)

        


class AuthUser(HttpUser):
    weight = 5
    tasks = [UserLogedin]
    last_wait_time = 0
    reach_limit = False

    def wait_time(self):
        if self.last_wait_time < 2:
            self.last_wait_time += 0.1
        else:
            self.reach_limit = True
        return self.last_wait_time

class user(HttpUser):
    weight = 10
    tasks = [userRedirect]
    last_wait_time = 2
    reach_limit = False

    def wait_time(self):
        if self.last_wait_time > 0.1:
            self.last_wait_time -= 0.1
        else:
            self.reach_limit = True
        return self.last_wait_time