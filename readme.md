# Url Shortener

Url shortener is a service in django for shortening long urls.



## Apis

```
/docs/ #get a complete list of urls
/report/per-url/ #get per url analytics data
/report/per-user/ #get per user analytics data
/uApi/ #get list of your apis
/uApi/r/short-url/ #redirect to long url
/uApi/rest-auth/login/ #login
/uApi/rest-auth/logout/ #logout
/uApi/rest-auth/registration/ #sign up

```