from rest_framework import serializers

from ..models import AnalyticsPerUrl


class AnalyticsPerUrlSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['urlData', 'report', 'requestCount', 'platformCount', 'browserCount', 'createdAt']
        model = AnalyticsPerUrl
