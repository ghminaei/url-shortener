from rest_framework import serializers

from ..models import AnalyticsPerUser


class AnalyticsPerUserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['urlData', 'report', 'requestCount', 'platformCount', 'browserCount', 'createdAt']
        model = AnalyticsPerUser
