from rest_framework import generics

from ..models import AnalyticsPerUser
from ..serializers import AnalyticsPerUserSerializer

class AnalyticsPerUserList(generics.ListAPIView):
    serializer_class = AnalyticsPerUserSerializer

    def get_queryset(self):
        user = self.request.user
        return AnalyticsPerUser.objects.filter(urlData__owner=user)
