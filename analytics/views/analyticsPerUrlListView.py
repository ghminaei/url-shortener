from rest_framework import generics

from ..models import AnalyticsPerUrl
from ..serializers import AnalyticsPerUrlSerializer

class AnalyticsPerUrlList(generics.ListAPIView):
    serializer_class = AnalyticsPerUrlSerializer

    def get_queryset(self):
        user = self.request.user
        return AnalyticsPerUrl.objects.filter(urlData__owner=user)
