from django.urls import path
from .views import AnalyticsPerUserList, AnalyticsPerUrlList
urlpatterns = [
    path('per-user/', AnalyticsPerUserList.as_view()),
    path('per-url/', AnalyticsPerUrlList.as_view()),
]