from django.db import models
from django.contrib.postgres.fields import JSONField

class AnalyticsPerUser(models.Model):
    DAILY = 'DA'
    YESTERDAY = 'YE'
    WEEKLY = 'WE'
    MONTHLY = 'MO'
    REPORT_TYPE_CHOICES = [
        (DAILY, 'Daily'),
        (YESTERDAY, 'Yesterday'),
        (WEEKLY, 'Weekly'),
        (MONTHLY, 'Monthly')
    ]
    urlData = models.ForeignKey('urlApis.UrlData', on_delete=models.CASCADE)
    report = models.CharField(
        max_length=2,
        choices=REPORT_TYPE_CHOICES,
        default=DAILY
    )
    requestCount = models.IntegerField(default=0)
    platformCount = JSONField()
    browserCount = JSONField()
    createdAt = models.DateTimeField(auto_now=True)
