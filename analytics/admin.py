from django.contrib import admin
from .models import AnalyticsPerUser, AnalyticsPerUrl

admin.site.register(AnalyticsPerUser)
admin.site.register(AnalyticsPerUrl)