from datetime import timedelta, time, datetime

from .reportBaseCommand import ReportBaseCommand
from django.utils import timezone
from django.utils.timezone import make_aware
from analytics.models import AnalyticsPerUrl

now = timezone.localtime(timezone.now())
month = now - timedelta(30)
monthStart = make_aware(datetime.combine(month, time()))
todayStart = make_aware(datetime.combine(now, time()))

class Command(ReportBaseCommand):
    help = "Update monthly report"

    def handle(self, *args, **options):
        requests = self.getDataInRange(monthStart, todayStart)
        if requests:
            self.handleCommand(requests, AnalyticsPerUrl.MONTHLY)
