import json

from django.core.management import BaseCommand


from urlApis.models import UrlRequests
from analytics.models import AnalyticsPerUrl, AnalyticsPerUser


class ReportBaseCommand(BaseCommand):

    def createOrUpdateModel(self, model, urlData, val, reportType):
        entry = model.objects.filter(
            urlData=urlData, report=reportType).first()
        if entry:
            entry.requestCount = val['requestCount']
            entry.platformCount = json.dumps(val['platformCount'])
            entry.browser = json.dumps(val['browserCount'])
            entry.save()
        else:
            model.objects.create(
                urlData = urlData,
                report = reportType,
                requestCount = val['requestCount'],
                platformCount = val['platformCount'],
                browserCount = val['browserCount']
            )
    def getDataInRange(self, start, end):
        return UrlRequests.objects.filter(
            accessedAt__range=(start, end))
    
    def handleCommand(self, requests, reportType):
        dataPerUrl = {}
        dataPerUser = {}
        for req in requests:
            if not req.urlData in dataPerUrl:
                dataPerUrl[req.urlData] = {
                    'requestCount': 0,
                    'platformCount': {},
                    'browserCount': {},
                }
                
            self.prepareData(req, dataPerUrl)

            if not req.urlData in dataPerUser:
                dataPerUser[req.urlData] = {
                    'users': [],
                    'requestCount': 0,
                    'platformCount': {},
                    'browserCount': {},
                }
            
            if not self.isUserExist(req, dataPerUser[req.urlData]['users']):
                dataPerUser[req.urlData]['users'].append(
                    self.getUserIndicator(req))
                self.prepareData(req, dataPerUser)

        for key, val in dataPerUrl.items():
            self.createOrUpdateModel(AnalyticsPerUrl, key, val, reportType)
        
        for key, val in dataPerUser.items():
            self.createOrUpdateModel(AnalyticsPerUser, key, val, reportType)
            
    def isUserExist(self, req, userList):
        if req.user:
            return req.user in userList
        return req.session in userList

    def getUserIndicator(self, req):
        return req.user if req.user else req.session

    def prepareData(self, req, data):
        data[req.urlData]['requestCount'] += 1

        if req.browserFamily in data[req.urlData]['browserCount']:
            data[req.urlData]['browserCount'][req.browserFamily] += 1
        else:
            data[req.urlData]['browserCount'][req.browserFamily] = 1
        
        if req.platform in data[req.urlData]['platformCount']:
            data[req.urlData]['platformCount'][req.platform] += 1
        else:
            data[req.urlData]['platformCount'][req.platform] = 1
    