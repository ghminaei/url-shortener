from datetime import timedelta, time, datetime

from .reportBaseCommand import ReportBaseCommand
from django.utils import timezone
from django.utils.timezone import make_aware
from analytics.models import AnalyticsPerUrl

now = timezone.localtime(timezone.now())
yesterday = now - timedelta(1)
yesterdayStart = make_aware(datetime.combine(yesterday, time()))
todayStart = make_aware(datetime.combine(now, time()))

class Command(ReportBaseCommand):
    help = "Update yesterday report"

    def handle(self, *args, **options):
        requests = self.getDataInRange(yesterdayStart, todayStart)
        if requests:
            self.handleCommand(requests, AnalyticsPerUrl.YESTERDAY)
