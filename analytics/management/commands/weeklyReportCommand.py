from datetime import timedelta, time, datetime

from .reportBaseCommand import ReportBaseCommand
from django.utils import timezone
from django.utils.timezone import make_aware
from analytics.models import AnalyticsPerUrl

now = timezone.localtime(timezone.now())
week = now - timedelta(7)
weekStart = make_aware(datetime.combine(week, time()))
todayStart = make_aware(datetime.combine(now, time()))

class Command(ReportBaseCommand):
    help = "Update weekly report"

    def handle(self, *args, **options):
        requests = self.getDataInRange(weekStart, todayStart)
        if requests:
            self.handleCommand(requests, AnalyticsPerUrl.WEEKLY)
