from datetime import timedelta, time, datetime

from .reportBaseCommand import ReportBaseCommand
from django.utils import timezone
from django.utils.timezone import make_aware
from analytics.models import AnalyticsPerUrl

now = timezone.localtime(timezone.now())
tomorrow = now + timedelta(1)
todayStart = make_aware(datetime.combine(now, time()))

class Command(ReportBaseCommand):
    help = "Update daily report"

    def handle(self, *args, **options):
        requests = self.getDataInRange(todayStart, now)
        if requests:
            self.handleCommand(requests, AnalyticsPerUrl.DAILY)
