from django.contrib import admin
from django.urls import path, include

from rest_framework.documentation import include_docs_urls
from rest_framework.schemas import get_schema_view

schemaView = get_schema_view(title='Blog API')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('uApi/', include('urlApis.urls')),
    path('uApi/rest-auth/', include('rest_auth.urls')),
    path('uApi/rest-auth/registration/',
        include('rest_auth.registration.urls')),
    path('report/', include('analytics.urls')),
    path('docs/', include_docs_urls(title='Url Shortener')),
    path('', include_docs_urls(title='Url Shortener')),
    path('schema/', schemaView),
]
