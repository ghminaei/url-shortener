from celery import shared_task
from django.core import management

@shared_task
def runDailyReportCommand():
    management.call_command("dailyReportCommand", )

@shared_task
def runYesterdayReportCommand():
    management.call_command("yesterdayReportCommand", )

@shared_task
def runWeeklyReportCommand():
    management.call_command("weeklyReportCommand", )

@shared_task
def runMonthlyReportCommand():
    management.call_command("monthlyReportCommand", )
