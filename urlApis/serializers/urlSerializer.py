from rest_framework import serializers
from ..models import UrlData

import string, random

SHORT_URL_LENGTH = 10

class UrlSerializer(serializers.ModelSerializer):
    shortUrl = serializers.ReadOnlyField()
    owner = serializers.HiddenField(
        default=serializers.CurrentUserDefault(),
    )

    class Meta:
        fields = ['longUrl', 'shortUrl', 'owner', 'createdAt', 'updatedAt', 'suggestedUrl']
        model = UrlData
    
    def createNewShortUrl(self):
        s = string.ascii_uppercase + string.ascii_lowercase + string.digits
        urlId = ''.join(random.choices(s, k=SHORT_URL_LENGTH))
        if not UrlData.objects.filter(shortUrl=urlId).exists():
            return urlId
        else:
            return self.createNewShortUrl()

    def createCustomShortUrl(self, suggested, attempt=0):
        if attempt == 3:
            return self.createNewShortUrl()
        s = string.ascii_uppercase + string.ascii_lowercase + string.digits
        partUrl = ''.join(random.choices(s, k=SHORT_URL_LENGTH-len(suggested)))
        urlId = suggested + partUrl
        if not UrlData.objects.filter(shortUrl=urlId).exists():
            return urlId
        else:
            return self.createCustomShortUrl(suggested, attempt + 1)

    def create(self, validated_data):
        longUrl = validated_data.get('longUrl')
        owner = validated_data.get('owner')
        suggestedUrl = validated_data.get('suggestedUrl')
        if suggestedUrl:
            shortUrl = self.createCustomShortUrl(suggestedUrl)
        else:
            shortUrl = self.createNewShortUrl()

        obj, created = UrlData.objects.update_or_create(
            longUrl=longUrl, owner=owner,
            defaults={'shortUrl': shortUrl},
        )
        return obj



