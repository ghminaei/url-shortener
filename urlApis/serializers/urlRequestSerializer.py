from rest_framework import serializers
from ..models import UrlRequests

class UrlRequestSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ['urlData', 'accessedAt', 'browserFamily', 'platform', 'user', 'session']
        model = UrlRequests