from django.contrib import admin
from .models import UrlData, UrlRequests

admin.site.register(UrlData)
admin.site.register(UrlRequests)