from django.urls import path
from .views import UrlList, UrlDetail, UrlRedirect

urlpatterns = [
    path('', UrlList.as_view()),
    path('<int:pk>/', UrlDetail.as_view()),
    path('r/<str:short>/', UrlRedirect.as_view()),
]