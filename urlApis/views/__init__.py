from .urlDetailView import UrlDetail
from .urlListView import UrlList
from .urlRedirectView import UrlRedirect