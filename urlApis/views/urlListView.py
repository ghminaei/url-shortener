from rest_framework import generics

from ..models import UrlData
from ..serializers import UrlSerializer

class UrlList(generics.ListCreateAPIView):
    serializer_class = UrlSerializer

    def get_queryset(self):
        user = self.request.user
        return UrlData.objects.filter(owner=user)

    