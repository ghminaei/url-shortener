from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.views import APIView

from django.core.cache import cache
from django.shortcuts import get_object_or_404

from ..models import UrlData
from ..serializers import UrlRequestSerializer

class UrlRedirect(APIView):
    permission_classes = [permissions.AllowAny]

    def createNewUrlRequest(self, request, urlObj):
        platform = 'mobile' if request.user_agent.is_mobile or request.user_agent.is_tablet else 'desktop'
        if not request.session.session_key:
            request.session.save()
        data = {
           'urlData': urlObj.pk,
           'browserFamily': request.user_agent.browser.family,
           'platform': platform,
           'session': request.session.session_key,
           'user': request.user.pk,
        }
        serializer = UrlRequestSerializer(data=data)
        if serializer.is_valid():
            serializer.save()

    def get(self, *args, **kwargs):
        shortUrl = self.kwargs.get('short')
        urlObj = cache.get(shortUrl)
        if not urlObj:
            urlObj = get_object_or_404(UrlData, shortUrl=shortUrl)
            cache.set(shortUrl, urlObj)
        data = {
            'redirect_url': urlObj.longUrl
        }
        self.createNewUrlRequest(self.request, urlObj)
        return Response(data)
    