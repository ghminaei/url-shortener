from rest_framework import generics

from ..models import UrlData
from ..serializers import UrlSerializer
from ..permissions import IsOwner

class UrlDetail(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsOwner]
    serializer_class = UrlSerializer

    def get_queryset(self):
        user = self.request.user
        return UrlData.objects.filter(owner=user)
    