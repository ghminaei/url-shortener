from django.db import models
from django.contrib.auth import get_user_model

class UrlData(models.Model):
    longUrl = models.CharField(max_length=200)
    shortUrl = models.CharField(max_length=20, unique=True)
    suggestedUrl = models.CharField(max_length=10, blank=True, default='')
    owner = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    createdAt = models.DateTimeField(auto_now=True)
    updatedAt = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.shortUrl} => {self.longUrl}"
