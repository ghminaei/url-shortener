from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.sessions.models import Session

from .urlDataModel import UrlData

class UrlRequests(models.Model):
    urlData = models.ForeignKey(UrlData(), on_delete=models.CASCADE)
    accessedAt = models.DateTimeField(auto_now=True)
    browserFamily = models.CharField(max_length=25)
    platform = models.CharField(max_length=25)
    user = models.ForeignKey(get_user_model(), null=True, blank=True, on_delete=models.CASCADE)
    session = models.CharField(max_length=40, blank=True, default='')
